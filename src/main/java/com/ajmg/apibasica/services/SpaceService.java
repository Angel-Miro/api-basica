package com.ajmg.apibasica.services;


import com.ajmg.apibasica.entities.Space;
import com.ajmg.apibasica.repositories.SpaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class SpaceService {
    @Autowired
    SpaceRepository spaceRepository;

    public List<Space> getSpace() {
        return spaceRepository.findAll();
    }

    public Space getSpaceById(int spaceId) {
        return spaceRepository.findById(spaceId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,
                String.format("The element with id %d was not found", spaceId)));
    }

    public Space postSpace(Space space) {
        return spaceRepository.save(space);
    }

    public Space putSpace(Space space) {
        Optional<Space> obj = spaceRepository.findById(space.getId());
        if (obj.isPresent()) {
            obj.get().setGalaxy(space.getGalaxy());
            obj.get().setPlanet(space.getPlanet());
            return spaceRepository.save(obj.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("The element %d was not found", space.getId()));
        }
    }

    public void deleteSpace(int spaceId) {
        Optional<Space> obj = spaceRepository.findById(spaceId);
        if (obj.isPresent()) {
            spaceRepository.delete(obj.get());
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("The element %d was not found", spaceId));
        }

    }
}
