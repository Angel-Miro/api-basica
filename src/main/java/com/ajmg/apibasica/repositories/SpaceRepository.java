package com.ajmg.apibasica.repositories;

import com.ajmg.apibasica.entities.Space;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpaceRepository extends JpaRepository<Space, Integer> {
}
