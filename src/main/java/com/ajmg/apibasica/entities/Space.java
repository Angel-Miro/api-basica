package com.ajmg.apibasica.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "space")
public class Space {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "galaxy")
    private String galaxy;

    @Column(name = "planet")
    private String planet;

    public Space() {}

    public Space(String galaxy, String planet){
        this.galaxy = galaxy;
        this.planet = planet;
    }



}