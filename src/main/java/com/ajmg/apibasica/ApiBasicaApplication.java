package com.ajmg.apibasica;

import com.ajmg.apibasica.entities.Space;
import com.ajmg.apibasica.repositories.SpaceRepository;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBasicaApplication implements ApplicationRunner {
	@Autowired
	private Faker faker;

	@Autowired
	private SpaceRepository spaceRepository;

	public static void main(String[] args) {
		SpringApplication.run(ApiBasicaApplication.class, args);

	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		for (int i = 0; i < 10; i++) {
			spaceRepository.save(new Space(faker.space().galaxy(),faker.space().planet()));
		}
	}
}
