package com.ajmg.apibasica.controllers;

import com.ajmg.apibasica.entities.Space;
import com.ajmg.apibasica.services.SpaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/space")
public class SpaceController {
    @Autowired
    SpaceService spaceService;

    @GetMapping(value = {"/",""})
    public ResponseEntity<List<Space>> getSpace(){
        return new ResponseEntity<>(spaceService.getSpace(), HttpStatus.OK);
    }

    @GetMapping(value = "/{spaceId}")
    public ResponseEntity<Space> getSpaceById(@PathVariable("spaceId") int spaceId){
        return new ResponseEntity<>(spaceService.getSpaceById(spaceId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Space> postSpace(@RequestBody Space space){
        return new ResponseEntity<>(spaceService.postSpace(space), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Space> putSpace(@RequestBody Space space){
        return new ResponseEntity<>(spaceService.putSpace(space), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{spaceId}")
    public ResponseEntity<Void> deleteSpace(@PathVariable("spaceId") int spaceId){
        spaceService.deleteSpace(spaceId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
